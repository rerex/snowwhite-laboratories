# Snowwhite Laboratories

Source code powering the Geocache "Snowwhite Laboratories - XY" https://coord.info/GC6P3PE

## Installation

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Start flask app
```
python app.py
```