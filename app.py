#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from logging import Formatter, FileHandler

import io
import os
import time
import hashlib
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from skimage import color, measure

from flask import Flask, render_template, flash, send_file
from flask_wtf import Form
from wtforms import StringField, SubmitField, validators
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)
#app.config.from_object('config')
app.config['SECRET_KEY'] = 'please_change!'
N_DIGITS = 5


class AnswerForm(Form):
    code = StringField('Please enter code:', validators=[validators.DataRequired()])
    submit_button = SubmitField('Validate')

    def validate(self):

        rv = Form.validate(self)
        if not rv:
            return False

        date = datetime.now().date().strftime("%Y%m%d")
        valid = self.code.data == str(get_key_for_date(date))
        if not valid:
            time.sleep(2)  # basic brute force protection
            self.code.errors.append('Wrong code')
        return valid


def get_key_for_date(date):
    return int(hashlib.md5(date.encode()).hexdigest(), 16) % 10**N_DIGITS


def string_to_point_cloud(input_string, n_points=None, debug=False):

    # create image from string and write it to memory file
    fig, axis = plt.subplots(1, 1, figsize=(len(input_string)*4, 4))
    axis.axis('off')
    axis.text(0, 0, input_string, fontsize=200, family="sans-serif", weight="bold")
    buf = io.BytesIO()
    plt.axis("tight")
    plt.savefig(buf, format='png')
    buf.seek(0)
    plt.close()

    # read image and reverse axes
    image = color.colorconv.rgb2grey(plt.imread(buf))
    image = image[::-1]

    # find contours and convert them to polygons
    contours = measure.find_contours(image, 0.8)
    contours = [measure.approximate_polygon(c, 0.8) for c in contours]
    polygons = [Path(list(zip(c[:, 1], c[:, 0]))) for c in contours]

    # plot contours and polygons
    if debug:
        fig, axis = plt.subplots(1, 1)

        for contour in contours:
            axis.plot(contour[:, 1], contour[:, 0], linewidth=2)

        for p in polygons:
            patch = PathPatch(p, facecolor='black', lw=2, alpha=0.5)
            axis.add_patch(patch)

    # find min and max x/y coordinates
    all_contour_points = np.vstack(contours)
    min_y, min_x = all_contour_points.min(axis=0)
    max_y, max_x = all_contour_points.max(axis=0)

    # uniformly sample the rectangle
    random_points = np.zeros((n_points, 2))
    random_points[:, 0] = np.random.uniform(min_x, max_x, size=n_points)
    random_points[:, 1] = np.random.uniform(min_y, max_y, size=n_points)

    if debug:
        axis.scatter(random_points[:, 0], random_points[:, 1], s=10, c="k", alpha=0.5)

    # only keep points within the polygons
    is_in_polygon = np.zeros((n_points), dtype=np.bool)
    for polygon in polygons:
        is_in_polygon ^= polygon.contains_points(random_points)

    if debug:
        axis.scatter(random_points[is_in_polygon, 0], random_points[is_in_polygon, 1], c="k", s=50)
        plt.show()

    return random_points[is_in_polygon, :]


def create_puzzle(input_string, output_file="puzzle.csv"):

    n_points = len(input_string) * 300
    points = string_to_point_cloud(input_string, n_points, debug=False)

    puzzle = np.hstack((points, np.random.permutation(points)))
    shuffled_index = np.random.permutation(np.arange(puzzle.shape[1]))

    np.savetxt(output_file, puzzle[:, shuffled_index], fmt="%.2f", delimiter="\t")


def create_puzzle_from_date(output_file="puzzle.csv"):

    date = datetime.now().date().strftime("%Y%m%d")
    date_file = "date.txt"

    # check if the work is already done
    if os.path.isfile(date_file) and os.path.isfile(output_file):
        with open(date_file) as date_file_handle:
            if date_file_handle.read().startswith(date):
                return

    print("Creating puzzle for " + date)

    key = get_key_for_date(date)
    # print(key)
    np.random.seed(key)
    create_puzzle(str(key), output_file)

    with open(date_file, "w") as date_file_handle:
        date_file_handle.write(date)


@app.route("/", methods=['GET', 'POST'])
def index():

    form = AnswerForm()
    if form.validate_on_submit():
        flash("Sample container located... 51°31.XXX 009°53.XXX", "info")
    return render_template('index.html', form=form)


@app.route('/puzzle/')
@app.route('/puzzle.csv')
def puzzle():
    create_puzzle_from_date("puzzle.csv")
    return send_file("puzzle.csv", as_attachment=True)


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')


if __name__ == '__main__':
    app.run()

